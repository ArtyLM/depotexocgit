#include <stdio.h>
#include <stdlib.h>

int main()
{
    int nbEleves = 0;
    float note = 0;
    int cpt = 0;
    float moyenne = 0;
    float i = 0;
    int min = 10;
    int max = 10;

    printf("\nSaisir nombre eleves : ");
    scanf("\n%d", &nbEleves);
    printf("\nNombre eleves : %d \n", nbEleves);

    while(cpt < nbEleves)
    {
        printf("\nSaisir note eleve : ");
        scanf("\n%f", &note);
        if(note < 0 || note > 20)
        {
            printf("Note hors limites!!!");
        }
        else
        {
            if(note < 10)
        {
            printf("\nEleve non-admis");
        }
        else
        {
            printf("\nEleve admis");
        }

        if(note < min)
        {
            min = note;
        }
        else
        {
            max = note;
        }

        cpt++;
        i = i + note;
        moyenne = i / cpt;
        printf("\nMoyenne : %.2f", moyenne);
        }

    }

    printf("\nNote min : %d", min);
    printf("\nNote max : %d", max);

    return 0;
}
